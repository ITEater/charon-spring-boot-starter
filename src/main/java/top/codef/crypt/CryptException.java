package top.codef.crypt;

public class CryptException extends RuntimeException {

	private static final long serialVersionUID = -6225823692487733126L;

	public CryptException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CryptException(String message, Throwable cause) {
		super(message, cause);
	}

	public CryptException(String message) {
		super(message);
	}

	public CryptException(Throwable cause) {
		super(cause);
	}
}
