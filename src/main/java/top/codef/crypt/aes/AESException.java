package top.codef.crypt.aes;

import top.codef.crypt.CryptException;

public class AESException extends CryptException {

	public AESException(String message) {
		super(message);
	}

	public AESException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AESException(String message, Throwable cause) {
		super(message, cause);
	}

	public AESException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = -3736456626108840626L;

}
