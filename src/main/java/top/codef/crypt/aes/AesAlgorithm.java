package top.codef.crypt.aes;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;

public enum AesAlgorithm {

	AES("AES"), AES_128_CBC_PKCS7("AES/CBC/PKCS7Padding"), AES_GCM_NOPADDING("AES/GCM/NoPadding");

	private final String algorithm;

	public String getAlgorithm() {
		return algorithm;
	}

	private AesAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public AlgorithmParameterSpec generrateParameterSpec(int bitLength, byte[] param) {
		switch (this) {
		case AES:
			return new IvParameterSpec(param);
		case AES_128_CBC_PKCS7:
			return new IvParameterSpec(param);
		case AES_GCM_NOPADDING:
			return new GCMParameterSpec(bitLength, param);
		}
		return null;
	}

}
