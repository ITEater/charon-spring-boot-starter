package top.codef.crypt.rsa;

public enum RsaAlgorithm {

	RSA("RSA"), RSA_ECB_OAEPWithSHA_1AndMGF1Padding("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");

	private final String value;

	private RsaAlgorithm(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
