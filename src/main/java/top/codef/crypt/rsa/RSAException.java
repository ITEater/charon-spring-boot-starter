package top.codef.crypt.rsa;

import top.codef.crypt.CryptException;

public class RSAException extends CryptException {

	public RSAException(String message) {
		super(message);
	}

	public RSAException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RSAException(String message, Throwable cause) {
		super(message, cause);
	}

	public RSAException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = -3736456626108840626L;

}
