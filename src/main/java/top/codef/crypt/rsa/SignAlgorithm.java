package top.codef.crypt.rsa;

public enum SignAlgorithm {

	MD5_WITH_RSA("MD5WithRSA"), SHA256_WITH_RSA("SHA256WithRSA");

	private final String signType;

	private SignAlgorithm(String signType) {
		this.signType = signType;
	}

	/**
	 * @return the signType
	 */
	public String getSignType() {
		return signType;
	}

}
