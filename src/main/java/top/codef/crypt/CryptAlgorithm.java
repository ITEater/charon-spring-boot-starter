package top.codef.crypt;

public enum CryptAlgorithm {

	RSA("RSA"), AES("AES"), AES_128_CBC_PKCS7("AES/CBC/PKCS7Padding");

	private final String algorithm;

	private CryptAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	/**
	 * @return the algorithm
	 */
	public String getAlgorithm() {
		return algorithm;
	}

}
