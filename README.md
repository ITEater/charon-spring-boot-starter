# charon-spring-boot-starter


![jdk版本](https://img.shields.io/badge/java-17%2B-red.svg?style=for-the-badge&logo=appveyor)
![maven版本](https://img.shields.io/badge/maven-3.2.5%2B-red.svg?style=for-the-badge&logo=appveyor)

#### 介绍
- 专门处理加密的框架，目前只是当做一个工具包进行处理，后续所有相关加密都会在这里维护
- master分支专门为springboot3.0+版本准备，低版本请看**0.1.0**分支
#### 软件架构

目前只支持了AES，与RSA加密

1. AES加密中支持了
   1. 基础AES加密
   2. AES/CBC/PKCS7Padding
   3. AES/GCM/NoPadding
2. RSA加密校验支持了
   1. MD5校验
   2. SHA256校验


#### 安装教程

1.  将此项目拉取到本地
2.  执行`mvn install`
3.  在其他项目的`pom.xml`中引入
```
    <dependency>
	<groupId>top.codef</groupId>
	<artifactId>charon-spring-boot-starter</artifactId>
	<version>0.2.0</version>
    <dependency>
```
